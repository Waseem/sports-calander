<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadExerciseData implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		\Nelmio\Alice\Fixtures::load(
			__DIR__ . '/fixtures.yml',
			$manager,
			['providers' => ['\AppBundle\DataFixtures\ORM\ExerciseDataProvider']])
		;
	}
}