<?php

namespace AppBundle\DataFixtures\ORM;

use Faker\Provider\DateTime as BaseProvider;

class ExerciseDataProvider extends BaseProvider
{

	/**
	 * @var array Array of exercises (short description)
	 */
	private static $descriptionProvider = [
		'Press of a bar',
		'Bumbbell bench press',
		'Barbell press seated incline',
	];

	/**
	 * Gets random short description of exercise
	 *
	 * @return string
	 */
	public static function description()
	{
		return self::randomElement(self::$descriptionProvider);
	}
}