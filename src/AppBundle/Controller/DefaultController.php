<?php

namespace AppBundle\Controller;

use AppBundle\Service\Exercise;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction(Request $request)
    {
        /** @var Exercise $service */
        $service = $this->get('app.exercise');

        // Render default view
        return array(
            'data' => $service->fetchDashboardData(),
        );
    }
}
