<?php

namespace AppBundle\Service;

use AppBundle\Entity\Exercise as TheEntity;
use AppBundle\Repository\ExerciseRepository;

class Exercise
{
	/**
	 * @var \AppBundle\Repository\ExerciseRepository
	 */
	private $repository;

	/**
	 * Exercise service constructor.
	 *
	 * @param \AppBundle\Repository\ExerciseRepository $exerciseRepository
	 */
	public function __construct(ExerciseRepository $exerciseRepository)
	{
		$this->repository = $exerciseRepository;
	}

	/**
	 * Fetches data for dashboard
	 *
	 * Fetches trainings performed today, same day last week and same day last-to-last week
	 *
	 * @return array
	 */
	public function fetchDashboardData()
	{
		$startTimestamp = strtotime('today');
		$dates = [
			'today' => date('Y-m-d', $startTimestamp),
			'weekAgo' => date('Y-m-d', strtotime('-7 days', $startTimestamp)),
			'twoWeeksAgo' => date('Y-m-d', strtotime('-14 days', $startTimestamp)),
		];

		$data = $this->repository->fetchTrainingsByDate($dates);

		$groupedData = array_fill_keys(array_keys($dates), []);

		foreach ($data as $exercise) {
			/** @var TheEntity $exercise */

			$date = $exercise->getDate()->format('Y-m-d');

			foreach ($dates as $segment => $segmentDate) {
				if ($segmentDate == $date) {
					$groupedData[$segment][] = $exercise;
				}
			}
		}

		return $groupedData;
	}
}