<?php

namespace AppBundle\Repository;

/**
 * Exercise Repository
 *
 * Houses Exercise entity related methods
 */
class ExerciseRepository extends \Doctrine\ORM\EntityRepository
{
	/**
	 * Fetches trainings performed for sought dates
	 *
	 * @param string|array $date
	 *
	 * @return array
	 */
	public function fetchTrainingsByDate($date)
	{
		return $this->findBy([
			'date' => $date
		]);
	}
}
