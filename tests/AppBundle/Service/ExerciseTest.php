<?php

namespace AppBundle\Service;

use AppBundle\Repository\ExerciseRepository;
use AppBundle\Entity\Exercise as ExerciseEntity;
use AppBundle\Service\Exercise;

class ExerciseTest extends \PHPUnit_Framework_TestCase
{
	public function testExerciseServiceHollow()
	{
		// Creating mock object
		$repositoryMock = $this->getRepositoryMock();

		// Instantiate target object with mock repository
		$testInstance = new Exercise($repositoryMock);

		$data = $testInstance->fetchDashboardData();

		$this->assertCount(3, $data);
		$this->assertArrayHasKey('today', $data, 'Data of "today" not available');
		$this->assertArrayHasKey('weekAgo', $data, 'Data of "week ago" not available');
		$this->assertArrayHasKey('twoWeeksAgo', $data, 'Data of "two weeks ago" not available');
	}

	public function testExerciseServiceWithData()
	{
		// Creating mock object
		$repositoryMock = $this->getRepositoryMock(
			$this->generateMockEntities()
		);

		// Instantiate target object with mock repository
		$testInstance = new Exercise($repositoryMock);

		$data = $testInstance->fetchDashboardData();

		$this->assertCount(3, $data);
		$this->assertCount(1, $data['today']);
		$this->assertCount(1, $data['weekAgo']);
		$this->assertCount(1, $data['twoWeeksAgo']);
	}

	private function getRepositoryMock($returnValue = [])
	{
		$repositoryMock = $this->getMockBuilder(ExerciseRepository::class)
			->disableOriginalConstructor()
			->getMock();

		$repositoryMock->expects($this->once())
			->method('fetchTrainingsByDate')
			->willReturn($returnValue);

		return $repositoryMock;
	}

	/**
	 * Generate mock objects of Exercise entity with various dates
	 *
	 * @return array
	 */
	private function generateMockEntities()
	{
		return [
			$this->createExerciseMock('today'),
			$this->createExerciseMock('yesterday'),
			$this->createExerciseMock('-1 week'),
			$this->createExerciseMock('-10 days'),
			$this->createExerciseMock('-14 days'),
			$this->createExerciseMock('-15 days'),
		];
	}

	/**
	 * Creates a mock object of Exercise entity with getDate function return specified date
	 *
	 * @param string $date Value equivalent of strtotime
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function createExerciseMock($date)
	{
		$entityMock = $this->getMockBuilder(ExerciseEntity::class)
			->disableOriginalConstructor()
			->getMock();

		$entityMock->expects($this->once())
			->method('getDate')
			->willReturn(new \DateTime($date));

		return $entityMock;
	}
}
